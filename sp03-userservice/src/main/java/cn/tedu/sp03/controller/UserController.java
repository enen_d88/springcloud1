package cn.tedu.sp03.controller;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonResult;
import org.apache.catalina.realm.UserDatabaseRealm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    //获取用户
    @GetMapping("/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId) {
        User user = userService.getUser(userId);
        return JsonResult.build().code(200).data(user);
    }
    //增加积分
    // http://localhost:8101/8/score?score=1000
    @GetMapping("/{userId}/score")
    public JsonResult<?> addScore(
            @PathVariable Integer userId,
            Integer score) {
        userService.addScore(userId, score);
        return JsonResult.build().code(200).msg("增加积分成功");
    }

    //处理浏览器自动请求的图标文件
    @GetMapping("/favicon.ico")
    public void ico() {
    }

}
